module.exports = function (api) {
  api.cache(true);
  return {
    presets: ["babel-preset-expo"],
    plugins: [
      [
        "module-resolver",
        {
          alias: {
            "@Navigation": "./src/navigation",
            "@Components": "./src/components",
            "@Screens": "./src/screens",
            "@Store": "./src/store",
            "@Assets": "./assets",
            "@Types": "./src/types",
            "@Hooks": "./src/hooks",
            "@Utils": "./src/utils",
            "@Constants": "./src/constants",
          },
          extensions: [".js", ".jsx", ".ts", ".tsx"],
        },
      ],
    ],
  };
};
